# Projet SR2I204 - Keycloak & PAM

> par Quentin LIEUMONT et Ivan IVANOV

## Projet

Avec le développement du FAI de Rezel, nous aimerions pouvoir gérer des adhésions.
Chaque adhérent a un compte sur une plateforme web, le backend choisit pour gérer l'authentification est [Keycloak][1].
L'idée serait de pouvoir permettre à certains membres (admins) de se connecter à des serveurs sous Debian avec leur identifiants Keycloak, pour effectuer des opérations de maintenance.
Pour cela, nous avons imaginé une solution sous forme d'un module PAM qui communiquerait avec Keycloak. On utilisera peut être sssd comme intermédiaire.
Le but du projet sera donc de vérifier la faisabilité d'une telle solution, d'implémenter les solutions existantes voire de créer notre propre solution.

## Roadmap

- Mise en place d'un serveur Keycloak
- Étude de PAM
- Intégration de Keycloak en tant que module d'authentification PAM

## Mise en place et configuration d'un serveur Keycloak

[Keycloak][1] est produit open-source développé par Redhat. Il offre une solution SSO (Single-Sign On) en utilisant les protocoles OpenID Connect, OAuth 2.0 et SAML 2.0.

Commençons par créer un realm `servers`. Nous allons ensuite y créer plusieurs clients (chaque client représente une machine à laquelle on veut se connecter) et plusieurs utilisateurs.
Sur chaque client il faut activer la fonctionnalité `OAuth 2.0 Device Authorization Grant Enabled` pour permettre le _device-flow_.

Ensuite nous allons créer des rôles globaux et au niveau de clients.
Chaque utilisateur hérite de ses rôles locaux (dépendant de la machine à laquelle il se connecte) ainsi que globaux (indépendant de la machine) lors chaque connexion.

Le rôle `SSH` est réservé, si l'utilisateur a ce rôle, il peut ouvrir une session SSH, s'il ne dispose pas de ce rôle, l'ouverture de session échoue avec le code `Permission denied`.
Sinon chaque autre rôle lui est attribue un groupe unix du même nom.

On peut donc avoir des utilisateurs :

- qui peuvent se connecter et sont dans le groupe `sudo` sur toutes les machines (`SSH` & `sudo` en global),
- ou alors la permission de se SSH parout mais root sur une seule machine (`SSH` global & `sudo` sur un client),
- ou même plus évolué (`SSH` global, `wireshark` sur host 1 et `docker`  & `sudo` sur host 2).

Keycloak permet aussi la double authentification. Chaque utilisateur peut ainsi ajouter un TOTP s'il le désire en allant sur la page de son compte (`https://keycloak.example.com/auth/realms/servers/account/#/security/signingin`) puis activer `Two-Factor Authentication`. À chaque nouvelle connexion, un code TOTP lui sera demandé.

## Étude de PAM

### Un authentification unifiée

PAM est un standard décrit en 1995 par le [RFC 86.0](http://www.opengroup.org/rfc/rfc86.0.html) et implémenté par [Linux-PAM][12] en 1996.

Il a transformé les méthodes d'authentification avec un modèle assez novateur pour l'époque. PAM est une librairie qui fournissait aux application une API qui faisait le lien avec une ou plusieurs méthodes d'authentification sous forme de modules.
Ce standard a ainsi amené 2 avantages principaux :

- Les développeurs d'applications n'ont plus à implémenter chaque nouveau standard d'authentification, ils ont simplement à faire appel à PAM.
- On peut ajouter de nouvelles méthodes d'authentification à Linux "à la volée", sans avoir à recompiler Linux ou même PAM. Il suffit d'écrire un module PAM et l'ajouter à la configuration.

### Un module PAM

PAM se configure dans `/etc/pam.conf` ou via des fichiers dans `/etc/pam.d/` (méthode recommandée).

Reprenons l'exemple d'[Andrew Morgan][3], le fichier `/etc/pam.d/login` :

```bash
#%PAM-1.0
#(The above "magic" header is optional)
# The modules for login as they might appear in
# "/etc/pam.d/login" this configuration is
# accepted by Linux-PAM-0.56 and higher.
#
auth      requisite    pam_securetty.so
auth      required     pam_unix_auth.so
account   required     pam_unix_acct.so
session   optional     pam_cfs.so keys=/etc/security/cfs
session   required     pam_unix_sess.so
password  sufficient   pam_unix_passwd.so
password  required     pam_warn.so
# end of file.
```

Ce fichier définit les modules PAM à appeler par l'application `login` pour les 4 services de PAM :

- _Authentication_ : définit les méthodes qui permettent de vérifier l'identité de l'utilisateur
- _Account Management_ : vérifie la validité du compte de l'utilisateur, ainsi que la permission d'utiliser cette application.
- _Session Management_ : configure la session de l'utilisateur à sa connexion et sa déconnexion.
- _Password Management_ : permet de changer le mot de passe du compte.

### Écriture du module

Nous développons donc notre propre module pam en C. Nous commençons par implémenter l'authentification.
Pour cela, nous devons définir une fonction `pam_sm_authenticate(pam_handle_t *handle, int flags, int argc, const char **argv)`, qui sera appelée par PAM au moment opportun. La fonction commence par demander à le username et le mot de passe à l'utilisateur, grâce aux fonctions PAM `pam_get_user(handle, &username, "USERNAME: ")` et `pam_get_authtok(handle, PAM_AUTHTOK, &password, "PASSWORD: ")`. On remarque l'avantage de passer par l'API PAM : on n'a même pas besoin de savoir quel services veut authentifier l'utilisateur, vu qu'on a pas besoin de communiquer avec ce service, tout passe par PAM.

Une fois ces deux informations récupérées, nous allons vérifier qu'elles correspondent bien aux identifiants de l'utilisateur. Nous allors pour cela utiliser l'API Open ID connect de Keycloak.

### OpenID Connect

OpenID Connect (OIDC) est une couche se greffant au dessus du protocole OAuth 2.0.
Ce dernier définit de nombreux scénarios d'authentification et d'accès à une ressource protégée. Notamment:

- Password: L'application demande les identifiants de l'utilisateur, puis les envoie au serveur pour obtenir un token d'autorisation.
- Authorization Code Grant: L'utilisateur est redirigé vers une URL d'authentification. Il s'identifie auprès du serveur gérant les identités, puis ce dernier le redirige vers l'application en passant un authorization code que l'application pourra échanger pour un token d'autorisation auprès du serveur d'identité.
- refresh token, permet de renouveller une connexion à partir d'un refresh_token sauvegardé par l'application cliente.

Ici, nous avons déjà les informations de l'utilisateur, et nous pouvons donc nous authentifier en lien direct avec le serveur (premier scénario : password). Nous allons donc utiliser les API OpenID Connect de Keycloak.

Nous créons d'abord un client pour notre module PAM sur le dashboard Keycloak.

On peut ensuite récupérer les credentials qui nous permettrons d'authentifier notre application cliente. Nous pourrons ensuite différencier sur quelle machine l'utilisateur essaie de se connecter en créant des clients pour chaque machine. Elles seront différenciées par leurs credentiels, et Keycloak nous permet d'assigner des permissions aux utilisateurs différentes sur chaque client (donc chaque machine utilisant le module PAM).

En une seule requête HTTP, Keycloak va nous vérifier que l'utilisateur a bien fournit des identifiants valides, et nous donner ses permissions liées à ce client.

Notre serveur Keycloak est accessible via le domaine keycloak.local.rezel.net

```bash
curl -s --request POST --url $KEYCLOAK_URL/auth/realms/servers/protocol/openid-connect/token \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data grant_type=password \
  --data client_secret=$CLIENT_SECRET \
  --data client_id=$CLIENT_ID \
  --data username=$USER \
  --data password=$PASS
```

Nous renvoie :

```json
{
  "access_token": "eyJh...ieYw",
  "expires_in": 300,
  "refresh_expires_in": 1800,
  "refresh_token": "eyJh...H8Io",
  "token_type": "Bearer",
  "not-before-policy": 0,
  "session_state": "a88e5356-a824-4768-8487-4dbd54764417",
  "scope": "profile email"
}
```

Après avoir expérimenté avec la commande curl, nous utilisons libcurl pour intégrer ces appels dans notre code en C.

```c=
CURL* easyhandle;
easyhandle = curl_easy_init(); 
char *data;
if(0 > asprintf(&data, "grant_type=password&client_id=PAM&client_secret=client_secret=SECRET&username=%s&password=%s", user, pass)) 
    return 10;
curl_easy_setopt(easyhandle, CURLOPT_POSTFIELDS, data);
printf("Sending curl with data : %s\n", data);
curl_easy_setopt(easyhandle, CURLOPT_URL, "https://keycloak.local.rezel.net/auth/realms/servers/protocol/openid-connect/token");
CURLcode res;
res = curl_easy_perform(easyhandle); 
free(data);
if(CURLE_OK == res) {
 return PAM_SUCCESS;
} else {
 return PAM_PERM_DENIED;
}
```

Si la requête nous renvoie un code _HTTP OK (200)_, c'est que l'utilisateur a bien accès à ce client, et nous pouvons donc l'indiquer à PAM.

[Lien du code source du module](https://git.rezel.net/number42/SR2I204/src/commit/706fe7e19dfea71ec77334466d8c60a3952c6d74/server/pam_keycloak.c)

### Device-flow

Lors du TP IAM, nous découvrons un scénario d'authentification très intéressant : le device-flow. Il est pensé pour les appareils qui n'ont pas de navigateur web intégré, ou une interface pas très accessible.

Le scénario est le suivant :

- L'utilisateur essaie de se connecter sur l'appareil limité
- L'appareil lui affiche un code simple à retenir et une URL de Keycloak (ou un QRCode)
- L'utilisateur se rend sur l'url avec un autre appareil, rentre le device code
- Il se connecte avec son compte Keycloak
- L'appareil limité est connecté à Keycloak

Sachant que le scénario password qu'on utilisait jusqu'à maintenant n'était pas recommandé, car il implique que l'utilisateur entre son mot de passe dans une application différente du serveur d'identité. Le scénario device-flow est donc plus sécurisé et plus pratique, nous décidons de l'utiliser.

Pour l'implémenter :

1. On demande de commencer l'authentification "device" auprès de Keycloak

```bash
curl -fs --request POST --url $KEYCLOAK_URL/auth/realms/servers/protocol/openid-connect/auth/device \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data grant_type=password \
  --data client_secret=$CLIENT_SECRET \
  --data client_id=$CLIENT_ID
```

2. On obtient une URL à envoyer à l'utilisateur `verification_uri_complete`, ainsi qu'un `device-code` qui nous permet d'identifier

```json
{
  "device_code": "yZXF4GFBCocSpxClTi8gDcczu-fR9Qi_fHEM-XKR3d8",
  "user_code": "YIFE-BIKW",
  "verification_uri": "https://keycloak.local.rezel.net/auth/realms/servers/device",
  "verification_uri_complete": "https://keycloak.local.rezel.net/auth/realms/servers/device?user_code=YIFE-BIKW",
  "expires_in": 600,
  "interval": 5
}
```

3. On interroge périodiquement Keycloak en attendant que l'utilisateur se connecte

```bash
curl -s --request POST --url $KEYCLOAK_URL/auth/realms/servers/protocol/openid-connect/token \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data grant_type=urn:ietf:params:oauth:grant-type:device_code \
    --data client_secret=$CLIENT_SECRET \
    --data client_id=$CLIENT_ID \
    --data device_code="$(cat $tmp_file | jq -r '.device_code')")
```

4. Tant que l'utilisateur n'a pas terminé, on obtient `authorization_pending`

```json
{
  "error": "authorization_pending",
  "error_description": "The authorization request is still pending"
}
```

6. Lorsqu'il s'est bien connecté, on obtient un token au format JWT (Javascript Web Token)

```json
{
  "access_token": "eyJh...expQ",
  "expires_in": 300,
  "refresh_expires_in": 1800,
  "refresh_token": "eyJh...vFfE",
  "token_type": "Bearer",
  "not-before-policy": 0,
  "session_state": "682a6536-c4ac-4f66-abf4-c881fac3f2ec",
  "scope": "profile email"
}
```

### pam_exec.so

Nous avons maintenant besoin d'extraire plus d'informations des réponses de Keycloak. Parser le JSON nous permettra aussi de récupérer les groupes de l'utilisateur pour lui assigner des permissions spéciales ou les ajouter à des groupes unix correspondants. Malheureusement nos compétences en C sont encore très limitées. Nous avons alors découvert _pam_exec.so_, un module PAM qui permet de faire appel à n'importe quel exécutable pendant une transaction PAM.

Notre module PAM devient donc un script en bash qui sera invoqué par le module _pam_exec.so_. Nous pouvons récupérer les informations données par PAM via des variables d'environnement : _PAM_TYPE_ définit le type de requête (_auth_, _account_, _open_session_...), _PAM_USER_ l'utilisateur etc.

### Scripts Bash

Un code source d'une installation d'exemple est disponible dans [build/install](https://git.rezel.net/number42/SR2I204/src/branch/master/build/install/).
Nous avons divisé notre programme en plusieurs scripts bash qui effectuent différentes fonctions:

- [oidc_env](https://git.rezel.net/number42/SR2I204/src/branch/master/build/install/oidc_env) contient simplement les credentials identifiant le client auprès de Keycloak. Tous les autres scripts vont faire `source oidc_env`
- [oidc_setup](https://git.rezel.net/number42/SR2I204/src/branch/master/build/install/oidc_setup) va initier un scénario device-flow en faisant un `curl` vers l'API de Keycloak.
- [get_qr](https://git.rezel.net/number42/SR2I204/src/branch/master/build/install/get_qr) va récupérer depuis la réponse d'**oidc_setup** l'url de connexion pour l'utilisateur. Il va ensuite afficher cette url au format texte ainsi que sous la forme d'un QRCode.
- [oidc_verify](https://git.rezel.net/number42/SR2I204/src/branch/master/build/install/oidc_verify) utilise le device code obtenu par **oidc_setup** pour vérifier auprès de Keycloak si l'utilisateur a terminé son processus d'authentification.
- [oidc_pam](https://git.rezel.net/number42/SR2I204/src/branch/master/build/install/oidc_pam) est le script qui va être appelé par **pam_exec**. Il implémente les services _auth_ et _session_. Voici son comportement :

#### pam_auth

> Lorsque PAM_TYPE=pam_auth

On veut vérifier que l'utilisateur s'authentifie correctement.
On va appeler consécutivement **oidc_setup**, **get_qr**, puis **oidc_verify** à intervalles régulier en attendant que l'utilisateur se connecte.
Une fois qu'il est bien authentifié, on vérifie qu'il est dans le groupe "SSH", donc qu'il a bien le droit de se connecter à ce serveur.
On vérifie ensuite qu'il possède un compte utilisateur sur la machine, sinon on le crée.

#### pam_session

> Lorsque PAM_TYPE=open_session

À l'ouverture de la session, on ajoute l'utilisateur aux groupes auxquels il appartient dans Keycloak (autres que le groupe SSH).

> Lorsque PAM_TYPE=close_session

À la fermeture, on supprime les fichiers temporaires de notre module PAM associés à cette session.

#### configuration PAM

La majorité des services utilisant PAM pour l'authentification utilisent la configuration du fichier `/etc/pam/common-auth` que nous allons donc modifier. Notre module effectue le même role qui est actuellement effectué par le module **pam_unix.so**, nous allons donc remplacer son entrée :

```bash
auth [success=1 default=ignore] pam_exec.so stdout seteuid /opt/pam/oidc_pam
# auth [success=1 default=ignore] pam_unix.so nullok
```

Nous passons à `pam_exec.so` les paramètres suivants:

- stdout, pour afficher la sortie de notre script bash à l'utilisateur
- seteuid, pour que notre script soit exécuté avec les mêmes droits que ceux de son propriétaire (root). Il pourra ainsi créer des utilisateurs sur la machine.
- `/opt/pam/oidc_pam`, qui est tout simplement le chemin de notre script

Nous modifions aussi /etc/pam/common-session pour y rajouter tout en haut :

```bash
session     required      pam_exec.so seteuid /opt/pam/oidc_pam
```

Notre script fonctionne maintenant parfaitement, on peut le tester en faisant `su ahmed`, un lien et QRCode s'affichent pour nous permettre d'ouvrir Keycloak. Après s'être authentifié dans le navigateur, PAM nous débloque et on entre bien dans notre session linux.

### Problème avec SSH

Lors de la connexion via SSH, l'option `stdout` ne fonctionne pas. Le client ssh ne reçoit pas les messages de PAM. On se dit qu'on peut utiliser une bannière SSH pour afficher ces messages.

On va donc installer _inetd superserver_ qui permet de définir le lancement de serveurs TCP / UDP via un fichier de configuration.
Pour nous la configuration est assez simple : lorsqu'un utilisateur se connecte en SSH, on veut changer la banière SSH puis le rediriger vers le serveur SSH. La configuration est donc la suivante:

```
ssh stream tcp nowait root /opt/pam/entrypoint /opt/pam/entrypoint
```

Lors d'une connexion TCP sur le port 'ssh' (le mapping port <-> service se trouve dans `/etc/services`), l'utilisateur `root` vas lancer la commande `/opt/pam/entrypoint`.

Dans ce fichier on va trouver les commandes suivantes :

```bash=
#!/bin/bash
# the entrypoint script
# invoked by inetd on a new connection to the ssh port

oidc_file="$(/opt/pam/setup_ssh)"

# setup oidc
/opt/pam/oidc_setup "${oidc_file}"

# retrive the qr code
/opt/pam/get_qr "${oidc_file}" > /etc/ssh/banner

# run sshd client
mkdir -p -m 700 /run/sshd
/usr/sbin/sshd -i
```

On va donc effectuer les premières étapes du device flow à la place de notre module PAM, puis écrire leur résultat dans `/etc/ssh/banner` ce qui va avoir pour effet de l'afficher dans son terminal lors du démarage du client ssh (`/usr/sbin/sshd -i`). Cette banière va donc contenir le lien/QRCode pour se connecter via Keycloak.

On modifie aussi notre module pam pour que ce dernier utilise le device token généré par la connexion `inetd`, et fasse uniquement l'étape **oidc_verify** en attendant que l'utilisateur se connecte.

### Standardisation

Notre module peut actuellement fonctionner avec n'importe quel serveur Keycloak, mais on veut le rendre compatible avec tous les serveurs d'identité qui implémentent OpenID Connect.

Pour cela, nous utilisons [OpenID Connect Discovery 1.0](https://openid.net/specs/openid-connect-discovery-1_0.html), une norme qui permet de récupérer tous les endpoints (les URL pour interagir avec l'API) OAuth 2.0 d'un domaine (_realm_). Une seule url contient un JSON avec la liste de ces endpoints. Sur notre Keycloak, c'est l'URL qui finit en [.well-known/openid-configuration](https://keycloak.local.rezel.net/auth/realms/servers/.well-known/openid-configuration). Notre module va ensuite récupérer dans cette liste les endpoints pour initier le _device-flow_ (`device_authorization_endpoint`), et vérifier le token (`token_endpoint`).

### Installation

On décide d'utiliser [ansible](https://docs.ansible.com/ansible/latest/index.html) afin d'installer facilement les dépendences ainsi que les différents scripts de notre module, de modifier le service SSH grâce à inetd et d'écrire les configurations PAM nécessaires.

On peut ainsi effectuer l'installation sur plusieurs serveurs à la suite. Il suffit de définir quelques variables de configuration pour chaque serveur dans le fichier [ansible/hosts.yml](https://git.rezel.net/number42/SR2I204/src/branch/master/ansible/hosts.yml) :

- **install_dir**: le dossier d'installation de nos scripts bash
- **tmp_dir**: le dossier qui contiendra les données temporaires de notre module
- **oidc_config_url**: l'url OpenID Connect Discovery 1.0 du domaine
- **client_id** et **client_secret**: les credentials du client associé à ce serveur

Ansible effectue ensuite automatiquement les tâches suivantes :

- Crée le dossier d'installation
- Installe et configure inetd superserver
- Désactive le serveur SSH
- Installe la bonne configuration du deamon ssh
- Installe les scripts et leurs dépendences
- Télécharge la définiton de l'application OIDC
- Configure PAM

## Bibliographie

1. [Documentation Keycloak][1]
2. [Code source Keycloak][2]
3. [PAM - _Andrew Morgan_, Linux Journal][3]
4. [PAM - _Susan Lauber_, RedHat][4]
5. [Video about Keycloak, _Sébastien Blanc_][5]
6. [Linux source code, _Linus Torvalds_][6]
7. [Write a PAM Module, _fedetask_][7]
8. [Github on Keycloak-PAM implementation][8]
9. [man pam_exec][9]
11. [sssd documentation][11]
12. [Linux-PAM][12]
13. [OpenID Connect Discovery 1.0][13]

[1]: https://www.keycloak.org/documentation "Documentation Keycloak"
[2]: https://github.com/keycloak/keycloak "Code source Keycloak"
[3]: https://www.linuxjournal.com/article/2120 "PAM - Andrew Morgan, Linux Journal"
[4]: https://www.redhat.com/sysadmin/pluggable-authentication-modules-pam "PAM - Susan Lauber, RedHat"
[5]: https://www.youtube.com/watch?v=RupQWmYhrLA "Video about Keycloak, Sébastien Blanc"
[6]: https://github.com/torvalds/linux "Linux source code, Linus Torvalds"
[7]: https://web.archive.org/web/20190523222819/https://fedetask.com/write-linux-pam-module/ "Write a PAM Module, fedetask"
[8]: https://github.com/zhaow-de/pam-keycloak-oidc "Github on Keycloak-PAM implementation"
[9]: https://linux.die.net/man/8/pam_exec "man pam_exec"
[11]: https://sssd.io/docs/introduction.html "sssd documentation"
[12]: https://github.com/linux-pam/linux-pam "Linux-PAM"
[13]: https://openid.net/specs/openid-connect-discovery-1_0.html "OpenID Connect Discovery 1.0"
